package swip.client;

import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.smartwallet.swipsdk.Customer;
import ru.smartwallet.swipsdk.Frame;
import ru.smartwallet.swipsdk.Loyalty;
import ru.smartwallet.swipsdk.MerchantCredentials;
import ru.smartwallet.swipsdk.MerchantCredentialsStatus;
import ru.smartwallet.swipsdk.NewOrderData;
import ru.smartwallet.swipsdk.OrderAmount;
import ru.smartwallet.swipsdk.OrderStatusUpdate;
import ru.smartwallet.swipsdk.PayOrder;
import ru.smartwallet.swipsdk.PaymentStatusUpdate;
import ru.smartwallet.swipsdk.Pincode;
import ru.smartwallet.swipsdk.RefundStatusUpdate;
import ru.smartwallet.swipsdk.SWiPServiceGrpc;

import java.util.Iterator;

public class SdkClient {

    private static final Logger logger = LogManager.getLogger();

    private ManagedChannel channel;
    private SWiPServiceGrpc.SWiPServiceBlockingStub serviceStub;

    public synchronized void connectToSdk(String host, int port) {
        if (channel == null) {
            logger.debug("Opening connection to SDK");
            logger.debug("Host: " + host);
            logger.debug("Port: " + port);
            channel = ManagedChannelBuilder.forAddress(host, port)
                    .usePlaintext()
                    .build();
            serviceStub = SWiPServiceGrpc.newBlockingStub(channel);
        }
    }

    public synchronized MerchantCredentialsStatus authorize(MerchantCredentials credentials) {
        return serviceStub.init(credentials);
    }

    public synchronized Iterator<Frame> getFrameStream() {
        return serviceStub.getFrameStream(Empty.getDefaultInstance());
    }

    public synchronized Customer identify(Pincode pincode) {
        return serviceStub.identify(pincode);
    }

    public synchronized OrderStatusUpdate createOrder(NewOrderData newOrderData) {
        return serviceStub.createOrder(newOrderData);
    }

    public synchronized PaymentStatusUpdate payOrder(PayOrder payOrder) {
        return serviceStub.pay(payOrder);
    }

    public synchronized OrderStatusUpdate calculateLoyalty(Loyalty loyalty) {
        return serviceStub.calculateLoyalty(loyalty);
    }

    public synchronized OrderStatusUpdate closeOrder(OrderAmount closeOrderData) {
        return serviceStub.closeOrder(closeOrderData);
    }

    public synchronized RefundStatusUpdate refundOrder(OrderAmount refundOrderData) {
        return serviceStub.refund(refundOrderData);
    }
}
