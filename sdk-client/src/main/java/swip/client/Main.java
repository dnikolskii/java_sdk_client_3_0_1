package swip.client;

import com.google.protobuf.ByteString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.smartwallet.swipsdk.Customer;
import ru.smartwallet.swipsdk.DetectionStatus;
import ru.smartwallet.swipsdk.Frame;
import ru.smartwallet.swipsdk.IdentificationCode;
import ru.smartwallet.swipsdk.MerchantCredentials;
import ru.smartwallet.swipsdk.MerchantCredentialsCode;
import ru.smartwallet.swipsdk.MerchantCredentialsStatus;
import ru.smartwallet.swipsdk.Pincode;
import swip.client.gui.ImagePanel;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

public class Main {

    private static final Logger logger = LogManager.getLogger();

    private static final String PROPERTIES_FILE_NAME = "sdk_client.properties";

    private static final String PROPERTY_SDK_HOST = "sdk.host";
    private static final String PROPERTY_SDK_PORT = "sdk.port";
    private static final String PROPERTY_STP_AUTH_ID_MERCHANT = "stp.auth.id.merchant";
    private static final String PROPERTY_STP_AUTH_ID_STORE = "stp.auth.id.store";
    private static final String PROPERTY_STP_AUTH_ID_CASH = "stp.auth.id.cash";
    private static final String PROPERTY_STP_AUTH_TOKEN = "stp.auth.token";
    private static final String PROPERTY_SDK_PROCESS_DETECTION_FRAME_RATE = "sdk.process.detection.frame_rate";
    private static final String PROPERTY_SDK_PROCESS_TIMEOUT = "sdk.process.timeout";

    private static void updateProperties(Properties properties) throws IOException {
        logger.debug("Loading properties");
        try (Reader r = new FileReader(new File(PROPERTIES_FILE_NAME))) {
            properties.load(r);
        }
        logger.debug("Properties loaded");
    }

    private static void connectToSdk(SdkClient sdkClient, Properties properties) {
        logger.debug("Connecting to SDK");

        String host = properties.getProperty(PROPERTY_SDK_HOST);
        int port = Integer.parseInt(properties.getProperty(PROPERTY_SDK_PORT));
        logger.debug("Host: " + host);
        logger.debug("Port: " + port);

        sdkClient.connectToSdk(host, port);
        logger.debug("Connected to sdk");
    }

    private static void authorizeToStp(SdkClient sdkClient, Properties properties) {
        logger.debug("Authorizing to STP");

        String merchantId = properties.getProperty(PROPERTY_STP_AUTH_ID_MERCHANT);
        String storeId = properties.getProperty(PROPERTY_STP_AUTH_ID_STORE);
        String cashId = properties.getProperty(PROPERTY_STP_AUTH_ID_CASH);
        String token = properties.getProperty(PROPERTY_STP_AUTH_TOKEN);

        logger.debug("Merchant id: " + merchantId);
        logger.debug("Store id: " + storeId);
        logger.debug("Cash id: " + cashId);
        logger.debug("Token: " + token);

        MerchantCredentials merchantCredentials = MerchantCredentials.newBuilder()
                .setMerchantId(merchantId)
                .setStoreId(storeId)
                .setCashId(cashId)
                .setToken(token)
                .build();

        MerchantCredentialsStatus status = sdkClient.authorize(merchantCredentials);

        MerchantCredentialsCode code = status.getCode();
        logger.debug("Merchant credentials code: " + code);
        if (!MerchantCredentialsCode.MERCHANT_CREDENTIALS_OK.equals(code)) {
            throw new RuntimeException("STP authorization failed: " + status.getErrorMessage());
        }

        logger.debug("Authorized to STP");
    }

    private static void logDetectionStatus(DetectionStatus detectionStatus) {
        logger.debug("Detection status: " + detectionStatus);
    }

    private static byte[] getBuffer(int length, AtomicReference<byte[]> bufferStore) {
        byte[] buffer = bufferStore.getAndSet(null);
        if (buffer == null || buffer.length < length) {
            buffer = new byte[length];
        }
        return buffer;
    }

    private static void consumeFrame(
            ByteString frame,
            AtomicReference<byte[]> bufferStore,
            ImagePanel imagePanel
    ) {
        int size = frame.size();
        logger.debug("Frame size: " + size);
        byte[] buffer = getBuffer(size, bufferStore);
        frame.copyTo(buffer, 0);
        imagePanel.setImage(Toolkit.getDefaultToolkit().createImage(buffer, 0, size));
        imagePanel.repaint();
        bufferStore.set(buffer);
    }

    private static boolean handleFrameStream(
            SdkClient sdkClient,
            ImagePanel imagePanel,
            Properties properties
    ) {
        logger.debug("Starting frame stream");

        double frameRate = Double.parseDouble(properties.getProperty(PROPERTY_SDK_PROCESS_DETECTION_FRAME_RATE));
        long timeout = Long.parseLong(properties.getProperty(PROPERTY_SDK_PROCESS_TIMEOUT)) * 1000;

        logger.debug("Frame rate: " + frameRate);
        logger.debug("Timeout: " + timeout);

        AtomicReference<byte[]> bufferStore = new AtomicReference<>();

        long pause = Math.round(1000 / frameRate);
        long start = System.currentTimeMillis();
        long prev = start;
        long cur;

        Iterator<Frame> frameStream = sdkClient.getFrameStream();

        while (frameStream.hasNext()) {
            if (timeout + (start - System.currentTimeMillis()) <= 0) {
                throw new RuntimeException("Detection timed out");
            }
            Frame frame = frameStream.next();
            DetectionStatus detectionStatus = frame.getDetectionStatus();
            switch (detectionStatus) {
                case BESTSHOT_FOUND:
                    logDetectionStatus(detectionStatus);
                    consumeFrame(frame.getFrame(), bufferStore, imagePanel);
                    return true;
                case UNRECOGNIZED:
                case DETECTION_FAILED:
                    logDetectionStatus(detectionStatus);
                    logger.error("Detection failed: " + frame.getErrorMessage());
                    return false;
                case DETECTION_TIMEOUT:
                    logDetectionStatus(detectionStatus);
                    logger.error("Detection timed out");
                    return false;
                default:
                    cur = System.currentTimeMillis();
                    if (cur - prev >= pause) {
                        prev = cur;
                        logDetectionStatus(detectionStatus);
                        consumeFrame(frame.getFrame(), bufferStore, imagePanel);
                    }
            }
        }
        return false;
    }

    public static void main(String[] args) throws Exception {

        SdkClient sdkClient = new SdkClient();
        Properties properties = new Properties();
        ImagePanel imagePanel = new ImagePanel();
        imagePanel.setImageWidth(300);
        imagePanel.setImageHeight(400);
        imagePanel.setPreferredSize(new Dimension(300, 400));

        GraphicsDevice[] screens = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
        GraphicsDevice device = screens[0];
        GraphicsConfiguration deviceConfig = device.getDefaultConfiguration();

        JFrame window = new JFrame("SDK client", deviceConfig);

        Container windowContentPane = window.getContentPane();
        windowContentPane.setLayout(new FlowLayout(FlowLayout.LEFT));
        windowContentPane.setPreferredSize(new Dimension(500, 500));
        windowContentPane.add(imagePanel);
        windowContentPane.repaint();
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);

        updateProperties(properties);

        connectToSdk(sdkClient, properties);

        authorizeToStp(sdkClient, properties);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (!handleFrameStream(sdkClient, imagePanel, properties)) {
                System.out.print("Repeat frame stream y/n: ");
                String str = br.readLine();
                if (!"y".equalsIgnoreCase(str)) {
                    return;
                }
            }

            String pin = "";
            boolean stop = false;
            while (!stop) {
                Customer customer = sdkClient.identify(Pincode.newBuilder().setPincode(pin).build());
                IdentificationCode code = customer.getCode();
                switch (code) {
                    case IDENT_OK:
                        logger.debug("Customer identified: " + customer.getId());
                        stop = true;
                        break;
                    case IDENT_PIN_REQUIRED:
                        System.out.print("Insert pin: ");
                        pin = br.readLine();
                        break;
                    case IDENT_INCORRECT_PIN:
                        System.out.print("Pin incorrect. Try again: ");
                        pin = br.readLine();
                        break;
                    default:
                        logger.error("Identification failed");
                        logger.error("Identification code: " + code);
                        logger.error("Error message: " + customer.getErrorMessage());
                        stop = true;
                }
            }
        }
    }
}
