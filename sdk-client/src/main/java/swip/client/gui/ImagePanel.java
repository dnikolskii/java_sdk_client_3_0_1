package swip.client.gui;

import javax.swing.*;
import java.awt.*;

public class ImagePanel extends JPanel {

    private volatile int imageWidth;
    private volatile int imageHeight;

    private volatile Image image;

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int width = this.imageWidth;
        int height = this.imageHeight;
        Image image = this.image;
        if (image != null) {
            g.drawImage(image, 0, 0, width, height, this);
        } else {
            g.clearRect(0, 0, width, height);
        }
    }
}
